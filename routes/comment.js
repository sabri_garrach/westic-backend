const express = require("express");

const commentRouter = express.Router();

const Transporter = require("../models/transporter");
const Comment = require("../models/comment");
const User = require("../models/user");

commentRouter
  .route("/")
  .get((req, res, next) => {
    Comment.find()
      .then(
        (result) => {
          if (result.length != 0) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else {
            let err = new Error("Empty!");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Comment.create(req.body)
      .then(
        (result) => {
          Transporter.findById(result[0].ratedTransporter).then((trans) => {
            if (trans.length != 0) {
              trans.comments.push(result[0].id);
              trans
                .save()
                .then((resultat) => {
                  User.findById(result[0].writer).then((userResult) => {
                    userResult.comments.push(result[0].id);
                    userResult
                      .save()
                      .then(() => {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(result);
                      })
                      .catch((err) => next(err));
                  });
                })
                .catch((err) => next(err));
            } else {
              let err = new Error("Empty!");
              err.status = 404;
              next(err);
            }
          });
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put operation not supported on /comment");
  })
  .delete((req, res, next) => {
    Comment.remove({})
      .then(
        (result) => {
          Transporter.find().then((transporter) => {
            transporter.forEach((element) => {
              element.comments = [];
              element.save();
            });
            User.find().then((user) => {
              user.forEach((element) => {
                element.comments = [];
                element.save();
              });
              console.log("all the comments deleted correctly");
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json(result);
            });
          });
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

commentRouter
  .route("/:commentId")
  .get((req, res, next) => {
    Comment.findById(req.params.commentId)
      .then(
        (result) => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Post operation not supported on /comment/" + req.params.transporterId
    );
  })
  .put((req, res, next) => {
    console.log(req.body);
    Comment.findByIdAndUpdate(
      req.params.commentId,
      { $set: req.body },
      { new: true }
    )
      .then((result) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(result);
      })
      .catch((err) => next(err));
  })
  .delete((req, res, next) => {
    Comment.findById(req.params.commentId).then((result) => {
      if (result != null) {
        Transporter.findByIdAndUpdate(
          result.ratedTransporter,
          { $pull: { comments: req.params.commentId } },
          { new: true }
        )
          .then(() => {
            console.log("comment deleted succefully from Transporter");
          })
          .catch((err) => next(err));
        User.findByIdAndUpdate(
          result.writer,
          { $pull: { comments: req.params.commentId } },
          { new: true }
        )
          .then(() => {
            console.log("comment deleted succefully from User");
          })
          .catch((err) => next(err));
        Comment.findByIdAndDelete(req.params.commentId)
          .then(
            (resultat) => {
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json(resultat);
            },
            (err) => next(err)
          )
          .catch((err) => next(err));
      } else {
        let err = new Error("comment not found");
        err.status = 404;
        next(err);
      }
    });
  });

module.exports = commentRouter;
