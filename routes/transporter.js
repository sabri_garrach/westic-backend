const express = require("express");

const Transporter = require("../models/transporter");
const Fees = require("../models/fees");
const { Promise } = require("mongoose");
const transporterRouter = express.Router();

transporterRouter.route("/getNonPayedTransporters/").get((req, res, next) => {
  var resultat = [];
  Transporter.find()
    .populate("fees")
    .exec()
    .then((result) => {
      result.forEach((obj) => {
        for (let index = 0; index < obj.fees.length; index++) {
          console.log(obj.fees[index].payed);
          if (obj.fees[index].payed.toString() == req.query.option) {
            resultat.push(obj);
            break;
          }
        }
      });
      res.statusCode = 200;
      res.setHeader("Content-Type", "application/json");
      res.json(resultat);
    });
});

transporterRouter
  .route("/")
  .get((req, res, next) => {
    setImmediate(() => {
      Transporter.find({})
        .then(
          (result) => {
            if (result.length != 0) {
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json(result);
            } else {
              let err = new Error("empty!");
              err.status = 404;
              next(err);
            }
          },
          (err) => next(err)
        )
        .catch((err) => next(err));
    });
  })
  .post((req, res, next) => {
    Transporter.create(req.body)
      .then(
        (result) => {
          console.log("transporter created ", result);
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put operation not supported on /transporter");
  })
  .delete((req, res, next) => {
    Transporter.remove({})
      .then(
        (result) => {
          console.log("all the transporters deleted correctly");
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

transporterRouter
  .route("/:transporterId")
  .get((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result == null) {
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Post operation not supported on /tranporter/" + req.params.transporterId
    );
  })
  .put((req, res, next) => {
    Transporter.findByIdAndUpdate(
      req.params.transporterId,
      {
        $set: req.body,
      },
      { new: true }
    )
      .then(
        (result) => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .delete((req, res, next) => {
    Transporter.findByIdAndRemove(req.params.transporterId)
      .then(
        (result) => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

transporterRouter
  .route("/:transporterId/trucks")
  .get((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result.trucks);
          } else {
            err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null) {
            result.trucks.push(req.body);
            result.save().then(
              (result) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else {
            err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Put operation not supported on /tranporter " +
        req.params.transporterId +
        "/trucks"
    );
  })
  .delete((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null) {
            for (let i = result.trucks.length - 1; i >= 0; i--) {
              result.trucks.id(result.trucks[i]._id).remove();
            }
            result.save().then(
              (result) => {
                console.log("all the trucks deleted correctly");
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else {
            let err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

transporterRouter
  .route("/:transporterId/trucks/:trucksId")
  .get((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null && result.trucks.id(req.params.trucksId) != null) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result.trucks.id(req.params.trucksId));
          } else if (result == null) {
            let err = new Error("transporter not found");
            err.status = 404;
            next(err);
          } else {
            let err = new Error("truck not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Post operation not supported on /transporter/" +
        req.params.transporterId +
        "/trucks/" +
        req.params.trucksId
    );
  })
  .put((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null && result.trucks.id(req.params.trucksId) != null) {
            if (req.body.type) {
              result.trucks.id(req.params.trucksId).type = req.body.type;
            } else if (req.body.matricule) {
              result.trucks.id(req.params.trucksId).matricule =
                req.body.matricule;
            } else if (req.body.pricePerKm) {
              result.trucks.id(req.params.trucksId).pricePerKm =
                req.body.pricePerKm;
            } else if (req.body.weightMax) {
              result.trucks.id(req.params.trucksId).weightMax =
                req.body.weightMax;
            } else if (req.body.numPatente) {
              result.trucks.id(req.params.trucksId).numPatente =
                req.body.numPatente;
            }
            result.save().then((result) => {
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json(result.trucks.id(req.params.trucksId));
            });
          } else if (result == null) {
            err = new Error("transporter not found");
            err.status = 404;
            next(err);
          } else {
            err = new Error("truck not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .delete((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null && result.trucks.id(req.params.trucksId) != null) {
            result.trucks.id(req.params.trucksId).remove();
            result.save().then(
              (result) => {
                console.log("all the trucks deleted correctly");
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else if (result == null) {
            let err = new Error("transporter not found");
            err.status = 404;
            next(err);
          } else {
            let err = new Error("truck not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

transporterRouter
  .route("/:transporterId/fees")
  .get((req, res, next) => {
    Fees.find({ transporterId: req.params.transporterId })
      .then(
        (result) => {
          if (result != null) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else {
            let err = new Error("fees Empty");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (trans) => {
          if (trans != null) {
            Fees.create(req.body).then((result) => {
              trans.fees.push(result.id);
              trans.save().then((resultat) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(resultat);
              });
            });
          } else {
            err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Put operation not supported on /transporter " +
        req.params.transporterId +
        "/fees"
    );
  })
  .delete((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null) {
            result.fees = [];
            result.save().then(
              (resultat) => {
                Fees.deleteMany({
                  transporterId: req.params.transporterId,
                }).then((resultatt) => {
                  console.log("all the fees list deleted correctly");
                  res.statusCode = 200;
                  res.setHeader("Content-Type", "application/json");
                  res.json(result);
                });
              },
              (err) => next(err)
            );
          } else {
            let err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

transporterRouter
  .route("/:transporterId/fees/:feesId")
  .get((req, res, next) => {
    Fees.findById(req.params.feesId)
      .then(
        (result) => {
          if (
            result != null &&
            result.transporterId == req.params.transporterId
          ) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else if (result == null) {
            let err = new Error("fees not found");
            err.status = 404;
            next(err);
          } else {
            let err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Post operation not supported on /transporter/" +
        req.params.transporterId +
        "/fees/" +
        req.params.feesId
    );
  })
  .put((req, res, next) => {
    Fees.findByIdAndUpdate(
      req.params.feesId,
      {
        $set: req.body,
      },
      { new: true }
    )
      .then(
        (result) => {
          if (
            result != null &&
            result.transporterId == req.params.transporterId
          ) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else if (result == null) {
            err = new Error("fees not found");
            err.status = 404;
            next(err);
          } else {
            err = new Error("transporter not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .delete((req, res, next) => {
    Transporter.findById(req.params.transporterId)
      .then(
        (result) => {
          if (result != null) {
            Fees.findOneAndDelete({ _id: req.params.feesId }).then(
              (resultat) => {
                Transporter.findByIdAndUpdate(
                  req.params.transporterId,
                  { $pull: { fees: req.params.feesId } },
                  { new: true }
                ).then((resultatt) => {
                  console.log("the fees deleted correctly");
                  res.statusCode = 200;
                  res.setHeader("Content-Type", "application/json");
                  res.json(resultatt);
                });
              }
            );
          } else if (result == null) {
            let err = new Error("transporter not found");
            err.status = 404;
            next(err);
          } else {
            let err = new Error("fees not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

module.exports = transporterRouter;
