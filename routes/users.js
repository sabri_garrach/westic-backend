var express = require("express");
var userRouter = express.Router();

var User = require("../models/user");

/* GET users listing. */
userRouter
  .route("/")
  .get((req, res, next) => {
    User.find({})
      .then(
        (result) => {
          if (result.length != 0) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else {
            let err = new Error("empty!");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    User.create(req.body)
      .then(
        (result) => {
          console.log("transporter created ", result);
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put operation not supported on /user");
  })
  .delete((req, res, next) => {
    User.remove({})
      .then(
        (result) => {
          console.log("all the transporters deleted correctly");
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )

      .catch((err) => next(err));
  });

module.exports = userRouter;
