const express = require("express");
const offerRouter = express.Router();

const Offer = require("../models/offer");
const User = require("../models/user");
offerRouter
  .route("/")
  .get((req, res, next) => {
    Offer.find()
      .then(
        (result) => {
          if (result.length != 0) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else {
            let err = new Error("empty!");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Offer.create(req.body)
      .then(
        (result) => {
          User.findById(result[0].writer).then((user) => {
            user.offers.push(result[0].id);
            user.save().then(() => {
              console.log("transporter created ", result);
              res.statusCode = 200;
              res.setHeader("Content-Type", "application/json");
              res.json(result);
            });
          });
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put operation not supported on /offer");
  })
  .delete((req, res, next) => {
    Offer.remove({})
      .then(
        (result) => {
          User.find().then((users) => {
            users.forEach((element) => {
              element.offers = [];
              element.save();
            });
          });
          console.log("all the offers deleted correctly");
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

offerRouter
  .route("/:offerId")
  .get((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result == null) {
            res.statusCode = 404;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          } else {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    res.statusCode = 403;
    res.end("Post operation not supported on /offer/" + req.params.offerId);
  })
  .put((req, res, next) => {
    Offer.findByIdAndUpdate(
      req.params.offerId,
      {
        $set: req.body,
      },
      { new: true }
    )
      .then(
        (result) => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .delete((req, res, next) => {
    Offer.findByIdAndRemove(req.params.offerId)
      .then(
        (result) => {
          if (result != null) {
            User.findByIdAndUpdate(
              result.writer,
              { $pull: { offers: req.params.offerId } },
              { new: true }
            ).then(() => {
              console.log("item deleted succefuly from user");
            });
          }
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(result);
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

offerRouter
  .route("/:offerId/items")
  .get((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result != null) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result.items);
          } else {
            err = new Error("item not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result != null) {
            result.items.push(req.body);
            result.save().then(
              (result) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else {
            err = new Error("offer not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .put((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Put operation not supported on /offer " + req.params.offerId + "/items"
    );
  })
  .delete((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result != null) {
            for (let i = result.items.length - 1; i >= 0; i--) {
              result.items.id(result.items[i]._id).remove();
            }
            result.save().then(
              (result) => {
                console.log("all the items deleted correctly");
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else {
            let err = new Error("items not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

offerRouter
  .route("/:offerId/items/:itemId")
  .get((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result != null && result.items.id(req.params.itemId) != null) {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result.items.id(req.params.itemId));
          } else if (result == null) {
            let err = new Error("offer not found");
            err.status = 404;
            next(err);
          } else {
            let err = new Error("item not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post((req, res, next) => {
    res.statusCode = 403;
    res.end(
      "Post operation not supported on /offer/" +
        req.params.offerId +
        "/items/" +
        req.params.itemId
    );
  })
  .put((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result != null && result.items.id(req.params.itemId) != null) {
            if (req.body.name) {
              result.items.id(req.params.itemId).name = req.body.name;
            } else if (req.body.weightKg) {
              result.items.id(req.params.itemId).weightKg = req.body.weightKg;
            } else if (req.body.width) {
              result.items.id(req.params.itemId).width = req.body.width;
            } else if (req.body.height) {
              result.items.id(req.params.itemId).height = req.body.height;
            } else if (req.body.length) {
              result.items.id(req.params.itemId).length = req.body.length;
            } else if (req.body.quantity) {
              result.items.id(req.params.itemId).quantity = req.body.quantity;
            } else if (req.body.delicate) {
              result.items.id(req.params.itemId).delicate = req.body.delicate;
            }
            result
              .save()
              .then(
                (result) => {
                  res.statusCode = 200;
                  res.setHeader("Content-Type", "application/json");
                  res.json(result.items.id(req.params.itemId));
                },
                (err) => next(err)
              )
              .catch((err) => next(err));
          } else if (result == null) {
            err = new Error("offer not found");
            err.status = 404;
            next(err);
          } else {
            err = new Error("item not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .delete((req, res, next) => {
    Offer.findById(req.params.offerId)
      .then(
        (result) => {
          if (result != null && result.items.id(req.params.itemId) != null) {
            result.items.id(req.params.itemId).remove();
            result.save().then(
              (result) => {
                console.log("item deleted correctly");
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else if (result == null) {
            let err = new Error("offer not found");
            err.status = 404;
            next(err);
          } else {
            let err = new Error("item not found");
            err.status = 404;
            next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

module.exports = offerRouter;
