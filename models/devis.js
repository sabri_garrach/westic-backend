const mongoose = require("mongoose");
const Schema= mongoose.Schema;
var Float = require('mongoose-float').loadType(mongoose);


const devisSchema = new Schema({
    transporterId:{
        type: Object.Types.ObjectId,
        ref:"Transporter"
    },
    userId:{
        type: Object.Types.ObjectId,
        ref:"User"
    },
    price:{
        type:Float
    },
    offer:{
        type: Object.Types.ObjectId,
        ref:"Offer"
    },
    payed:{
        type:Boolean,
        default:false
    }
    
},
{
    timestamps:true,
});

module.exports = mongoose.model('Devis', devisSchema);