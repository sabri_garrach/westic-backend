const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var Float = require("mongoose-float").loadType(mongoose);

const feesSchema = new Schema(
  {
    date: {
      type: Date,
    },
    transporterId: {
      type: Schema.Types.ObjectId,
      ref: "Transporter",
    },
    payed: {
      type: Boolean,
    },
    price: {
      type: Float,
    },
    offer: {
      type: Schema.Types.ObjectId,
      ref: "Offer",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Fees", feesSchema);
