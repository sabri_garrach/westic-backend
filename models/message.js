const mongoose = require("mongoose");
const Schema= mongoose.Schema;
var Float = require('mongoose-float').loadType(mongoose);


const messageSchema = new Schema({
    coversationId:{
        type:String
    },
    sender:{
        type:String
    },
    text:{
        type:String
    }
},
{
    timestamps:true,
});

module.exports = mongoose.model('Message', messageSchema);