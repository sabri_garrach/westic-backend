const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var Float = require("mongoose-float").loadType(mongoose);

const shippingSchema = new Schema({
  date: {
    type: Date,
  },
  governorate: {
    type: String,
  },
  region: {
    type: String,
  },
  time: {
    type: String,
  },
  typePlace: {
    type: String,
  },
});

const deliverySchema = new Schema({
  date: {
    type: Date,
  },
  governorate: {
    type: String,
  },
  region: {
    type: String,
  },
  time: {
    type: String,
  },
  typePlace: {
    type: String,
  },
});

const itemSchema = new Schema(
  {
    name: {
      type: String,
    },
    weightKg: {
      type: Float,
    },
    width: {
      type: Float,
    },
    height: {
      type: Float,
    },
    length: {
      type: Float,
    },
    volume: {
      type: Float,
    },
    quantity: {
      type: Number,
    },
    delicate: {
      type: Boolean,
    },
  },
  {
    timestamps: true,
  }
);

const offerSchema = new Schema(
  {
    writer: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    shipping: {
      date: {
        type: Date,
      },
      governorate: {
        type: String,
      },
      region: {
        type: String,
      },
      time: {
        type: String,
      },
      typePlace: {
        type: String,
      },
    },
    delivery: {
      date: {
        type: Date,
      },
      governorate: {
        type: String,
      },
      region: {
        type: String,
      },
      time: {
        type: String,
      },
      typePlace: {
        type: String,
      },
    },
    category: {
      type: String,
    },
    title: {
      type: String,
    },
    items: [
      {
        name: {
          type: String,
        },
        weightKg: {
          type: Float,
        },
        width: {
          type: Float,
        },
        height: {
          type: Float,
        },
        length: {
          type: Float,
        },
        volume: {
          type: Float,
        },
        quantity: {
          type: Number,
        },
        delicate: {
          type: Boolean,
        },
      },
    ],
    totalVolume: {
      type: Float,
    },
    description: {
      type: String,
    },
    quotations: [
      {
        type: Schema.Types.ObjectId,
        ref: "Devis",
      },
    ],
    expired: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Offer", offerSchema);
