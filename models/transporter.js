const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Role = require("./enumerations/Role");
var Float = require("mongoose-float").loadType(mongoose);
const truckSchema = new Schema(
  {
    type: {
      type: String,
    },
    matricule: {
      type: String,
    },
    pricePerKm: {
      type: Float,
    },
    weightMax: {
      type: Float,
    },
    numPatente: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const transporterSchema = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
    },
    password: {
      type: String,
    },
    birthDate: {
      type: Date,
    },
    phoneNumber: {
      type: Number,
    },
    avatar: {
      type: String,
    },
    cin: {
      type: Number,
    },
    bio: {
      type: String,
    },
    workZone: [
      {
        type: String,
      },
    ],
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: "Comment",
      },
    ],
    expedition: [
      {
        type: Schema.Types.ObjectId,
        ref: "Offer",
      },
    ],
    trucks: [truckSchema],
    services: [
      {
        type: String,
      },
    ],
    disponibilityPerWeek: {
      type: Number,
    },
    fees: [
      {
        type: Schema.Types.ObjectId,
        ref: "Fees",
      },
    ],
    enable: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Transporter", transporterSchema);
