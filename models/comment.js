const mongoose = require("mongoose");
var Float = require("mongoose-float").loadType(mongoose);
const Schema = mongoose.Schema;

const commentSchema = new Schema(
  {
    writer: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    ratedTransporter: {
      type: Schema.Types.ObjectId,
      ref: "Transporter",
    },
    content: {
      type: String,
    },
    rating: {
      type: Float,
      default: 0.0,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Comment", commentSchema);
