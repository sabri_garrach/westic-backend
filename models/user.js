const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Role = require("./enumerations/Role");
const userSchema = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    email: {
      type: String,
    },
    password: {
      type: String,
    },
    birthDate: {
      type: Date,
    },
    phoneNumber: {
      type: Number,
    },
    avatar: {
      type: String,
    },
    role: {
      type: String,
      default: Role.CLIENT,
    },
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: "Comment",
      },
    ],
    offers: [
      {
        type: Schema.Types.ObjectId,
        ref: "Offer",
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema);
