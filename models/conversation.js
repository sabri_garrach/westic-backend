const mongoose = require("mongoose");
const Schema= mongoose.Schema;
var Float = require('mongoose-float').loadType(mongoose);


const conversationSchema = new Schema({
    members:[{
        type:String
    }]
},
{
    timestamps:true,
});

module.exports = mongoose.model('Conversation', conversationSchema);